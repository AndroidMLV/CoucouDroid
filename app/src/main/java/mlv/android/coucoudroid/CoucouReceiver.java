package mlv.android.coucoudroid;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.widget.Toast;

/*
* Broadcast Receivers simply respond to broadcast messages from other applications or from the system
* itself. These messages are sometime called events or intents. For example, applications can also
* initiate broadcasts to let other applications know that some data has been downloaded to the device
* and is available for them to use, so this is broadcast receiver who will intercept this communication
* and will initiate appropriate action.
*/
public class CoucouReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Toast.makeText(context, "Alarm!!", Toast.LENGTH_LONG).show();

        MediaPlayer mPlayer = MediaPlayer.create(context, R.raw.coucou);
        mPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
        mPlayer.start();
    }
}
