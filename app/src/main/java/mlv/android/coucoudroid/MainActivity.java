package mlv.android.coucoudroid;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import android.os.SystemClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TimePicker;
import android.widget.Toast;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {
    private PendingIntent pendingIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /*
            An INTENT is an abstract description of an operation to be performed. The primary pieces
            of information in an intent are:
            * action -- The general action to be performed, such as ACTION_VIEW, ACTION_EDIT,
                ACTION_MAIN, etc.
            * data -- The data to operate on, such as a person record in the contacts database,
                expressed as a Uri.
        */
        Intent alarmIntent = new Intent(MainActivity.this, CoucouReceiver.class);

        /*
            A PendingIntent specifies an action to take in the future. It lets you pass a future Intent
            to another application and allow that application to execute that Intent as if it had the same
            permissions as your application, whether or not your application is still around when the Intent
            is eventually invoked.It is a token that you give to a foreign application which allows the
            foreign application to use your application’s permissions to execute a predefined piece of code.
        */
        pendingIntent = PendingIntent.getBroadcast(MainActivity.this, 0, alarmIntent, 0);

        // Link the buttons to the actions they must do
        findViewById(R.id.startAlarmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startAlarm();
            }
        });

        findViewById(R.id.stopAlarmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                stopAlarm();
            }
        });

        findViewById(R.id.stopAlarmButton).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                everyMinuteAlarm();
            }
        });
    }

    private void everyMinuteAlarm() {
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        // RTC -> Alarm time in System.currentTimeMillis() (wall clock time in UTC).
        manager.setRepeating(AlarmManager.RTC,
                SystemClock.currentThreadTimeMillis(),
                60*1000,
                pendingIntent);
    }

    private void startAlarm() {
        /*This class provides access to the system alarm services. These allow you to schedule your
        application to be run at some point in the future. When an alarm goes off, the Intent that
        had been registered(alarmIntent) for it is broadcast by the system, automatically starting the target
        application if it is not already running. Registered alarms are retained while the device is
        asleep (and can optionally wake the device up if they go off during that time), but will be
        cleared if it is turned off and rebooted. */
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);

        //Setup the alarm
        TimePicker timePicker = (TimePicker) findViewById(R.id.timePicker);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, timePicker.getHour());
        calendar.set(Calendar.MINUTE, timePicker.getMinute());

        //Schedule an alarm to be delivered precisely at the stated time.
        manager.setExact(AlarmManager.RTC, calendar.getTimeInMillis(), pendingIntent);
    }

    private void stopAlarm() {
        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        manager.cancel(pendingIntent);
        Toast.makeText(this, "Alarm Canceled", Toast.LENGTH_SHORT).show();
    }
}
